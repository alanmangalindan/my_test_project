import mytestpackage.innertestpackage.ProgramInTestPackage;

import java.util.Arrays;
import java.util.List;

public class MyTestProgram {
    public void start() {
        System.out.println("Hello World from MySecondProgram");
    }

    public static void main(String[] args) {
        MyTestProgram p = new MyTestProgram();
        p.start();

        ProgramInTestPackage prog = new ProgramInTestPackage();
        prog.showVariables();
        ProgramInTestPackage prog2 = prog;
        prog2.showVariables();
        prog.setSampleInt(5);
        prog.setSampleString("I am a String that was set from MyTestProgram!");
        prog2.setSampleChar('p');
        prog.showVariables();
        prog2.showVariables();
        ProgramInTestPackage.showVariables();
        System.out.println(ProgramInTestPackage.sampleString);

        //        System.out.println((int)2.9 * 4.5);
//        System.out.println(Math.max(5,60) % Math.min(12,7));
//        System.out.println(0.2 * 3 / 2 + 3 / 2 * 3.2);

//        int a = 7;
//        int b = 1;
//        int c = a + 2;
//        a = b;
//        b = c;
//        c = c + 1;
//        System.out.println("a = " + a);
//        System.out.println("b = " + b);
//        System.out.println("c = " + c);

//        System.out.println("Lab 2 Exercise 3 Question 1: ");
//        System.out.println((int)2.9 * Double.parseDouble("4.5"));
//        System.out.println("17" + Integer.parseInt("2") * 3.5);
//        System.out.println("5 + 3" + 19 % 2 + 19 / 2);
//        System.out.println(2 + 5 + "59" + 3 * 2 + (3 + 2));
//
//        String colours, first, second, third;
//        int position1, position2, position3, length;
//        colours = "redorangeyellow";
//        first = colours.substring(4, 9);
//        second = colours.substring(0, 4);
//        third = colours.charAt(0) + colours.substring(13);
//        length = third.length();
//        third = third.toUpperCase();
//        position1 = colours.indexOf('A');
//        int position1_1 = colours.indexOf("BLAH");
//        position2 = colours.indexOf("el");
//        position3 = colours.indexOf("or");
//
//        System.out.println("Lab 2 Exercise 3 Question 2: ");
//        System.out.println("first: " + first);
//        System.out.println("second: " + second);
//        System.out.println("third: " + third);
//        System.out.println("length: " + length);
//        System.out.println("position1: " + position1);
//        System.out.println("position1_1: " + position1_1);
//        System.out.println("position2: " + position2);
//        System.out.println("position3: " + position3);
//
//        double radius;
//        System.out.println("\"Volume of a Sphere\"");
//        System.out.println("Enter the radius: ");
//        radius = Double.parseDouble("6.5");
//        double volume = (double) 4 / 3 * Math.PI * Math.pow(radius, 3);
//        System.out.println("Volume: " + volume);


//        System​.​out​.​println​((​int​)​2.9​ ​*​ ​4.5​);
//        System​.​out​.​println​((​int​)​2.9​ ​*​ ​4.5​);
//        System​.​out​.​println​(​Math​.​max​(​5​,​60​)​ ​%​ ​Math​.​min​(​12​,​7​));
//        System​.​out​.​println​(​0.2​ ​*​ ​3​ ​/​ ​2​ ​+​ ​3​ ​/​ ​2​ ​*​ ​3.2​);


//        Assignment 01 test codes:
//        String str = "Hello World!";
//        System.out.println(str.length());
//        char[] charArray = str.toCharArray();
//        char[] reverseCharArray = new char[charArray.length];
//        int endOfArray = charArray.length - 1;
//        for (int i = 0; i < charArray.length; i++) {
//            reverseCharArray[i] = charArray[endOfArray];
//            endOfArray--;
//        }
//        String reverseStr = String.valueOf(reverseCharArray);
//        System.out.println("str: "+ str);
//        System.out.println("reverseStr: " + reverseStr);
//
        int[] intArray = {22, 10, 9, 50, 1};
        for (int i = 0; i < intArray.length; i++) {
            boolean swapped = false;
            for(int j = 0; j < intArray.length - 1; j++) {
                if (intArray[j] > intArray[j + 1]) {
                    int tempInt = intArray[j];
                    intArray[j] = intArray[j + 1];
                    intArray[j + 1] = tempInt;
                    swapped = true;
                }
            }
            if (!swapped) {
                break;
            }
        }
        System.out.println(Arrays.toString(intArray));

        List<String> listToSort = Arrays.asList(new String[]{"E", "B", "C", "D", "A"});
        String[] arrayToSort = (String[]) listToSort.toArray();
        for (int i = 0; i < arrayToSort.length; i++ ) {
            boolean swapped = false;
            for (int j = 0; j < arrayToSort.length - 1; j++) {
                if (stringCompare(arrayToSort[j], arrayToSort[j + 1]) > 0) {
                    String temp = arrayToSort[j];
                    arrayToSort[j] = arrayToSort[j + 1];
                    arrayToSort[j + 1] = temp;
                    swapped = true;
                }
            }
            if (!swapped) {
                break;
            }
        }
        listToSort = Arrays.asList(arrayToSort);
        System.out.println(listToSort);

//
//        for (int i : intArray) {
//            System.out.println(i);
//        }
//
//        String intArrayString = "";
//        for (int i = 0; i < intArray.length; i++) {
//            if (i == intArray.length -1) {
//                intArrayString += intArray[i] + "";
//            }
//            else {
//                intArrayString += intArray[i] + ",";
//            }
//        }
//        System.out.println(intArrayString);
//
//        int num = 3;
//        String multiplicationTable = "";
//        for (int i = 1; i <= num; i++) {
//            for (int j = 1; j<= num; j++) {
//                if (j == num) {
//                    multiplicationTable += (j * i) + "\n";
//                } else {
//                    multiplicationTable += (j * i) + " ";
//                }
//            }
//        }
//        System.out.println(multiplicationTable);

//        int num = 20;
//        String primeNumList = "";
//        for (int i = 1; i <= num; i++) {
//            int ctr = 0;
//            for (int j = i; j >= 1; j--) {
//                if (i % j == 0) {
//                    ctr++;
//                }
//            }
//            if (ctr == 2) {
//                if (i != num) {
//                    primeNumList += i + " ";
//                } else {
//                    primeNumList += i + "";
//                }
//            }
//
//        }
//        primeNumList = primeNumList.trim();
//        System.out.println(primeNumList);


//        boolean isPalindrome = true;
//        int number = Math.abs(-101);
//        String s = Integer.toString(number);
//        char[] charArray = s.toCharArray();
//        int endOfArray = charArray.length - 1;
//        for (int i = 0; i < charArray.length; i++) {
//            if (charArray[i] == charArray[endOfArray]) {
//                endOfArray--;
//            } else {
//                isPalindrome = false;
//                System.out.println("isPalindrome: " + isPalindrome);
//                break;
//            }
//        }
//        System.out.println("isPalindrome: " + isPalindrome);

//        Part 3
//        int a = 12345;
//        int b = 678;
//        a = Math.abs(a); //ensures number is non-negative
//        b = Math.abs(b); //ensures number is non-negative
//        char[] aCharArray = Integer.toString(a).toCharArray();
//        char[] bCharArray = Integer.toString(b).toCharArray();
//        char[] zipCharArray = new char[aCharArray.length + bCharArray.length];
//        int smallestLength = Math.min(aCharArray.length, bCharArray.length);
//        for (int i = 0; i < smallestLength; i++) {
//            zipCharArray[2 * i] = aCharArray[i];
//            zipCharArray[2 * i + 1] = bCharArray[i];
//        }
//        if (aCharArray.length >= bCharArray.length) {
//            for (int i = 0; i < aCharArray.length - bCharArray.length; i++) {
//                zipCharArray[2 * smallestLength + i] = aCharArray[smallestLength + i];
//            }
//        } else {
//            for (int i = 0; i < bCharArray.length - aCharArray.length; i++) {
//                zipCharArray[2 * smallestLength + i] = bCharArray[smallestLength + i];
//            }
//
//        }
//        int zip = Integer.parseInt(String.valueOf(zipCharArray));

//        Part 3 Q4
//        int[] values = {-18, -1, 0, 8, 8, -1, 6, -18}; //{8, 6, 1, 8, 8, -1, -1, -1};
//        int[] count = new int[values.length];
//        for (int i = 0; i < values.length; i++) {
//            for (int j = 0; j < values.length; j++) {
//                if (values[i] == values[j]) {
//                    count[i]++;
//                }
//            }
//        }
//        int maxOccurrence = count[0]; // calls previous method defined in Q3
//        for (int i = 0; i < count.length; i++) {
//            if (maxOccurrence < count[i]) {
//                maxOccurrence = count[i];
//            }
//        }
//        int numberOfMaxOccurrences = 0;
//        for (int i = 0; i < count.length; i++) {
//            if (count[i] == maxOccurrence) {
//                numberOfMaxOccurrences++;
//            }
//        }
//        // use the array indicesOfMaxOccurrences to store indices where the max occurrences are in the values array
//        int[] indicesOfMaxOccurrences = new int[numberOfMaxOccurrences];
//        int j = 0;
//        for (int i = 0; i < count.length; i++) {
//            if (count[i] == maxOccurrence) {
//                indicesOfMaxOccurrences[j] = i;
//                j++;
//            }
//        }
//
//        int lowestMostFrequentInt = values[indicesOfMaxOccurrences[0]];
//        for (int i = 0; i < indicesOfMaxOccurrences.length; i++) {
//            if (lowestMostFrequentInt > values[indicesOfMaxOccurrences[i]]) {
//                lowestMostFrequentInt = values[indicesOfMaxOccurrences[i]];
//            }
//        }
//        System.out.println("lowestMostFrequentInt: " + lowestMostFrequentInt);

//        Part 3 Q5
//        int size = 0;
//        int[] fibonacciNumbers = new int[size];
//        for (int i = 0; i < fibonacciNumbers.length; i++) {
//            if (i == 0 || i == 1 ) {
//                fibonacciNumbers[i] = 1;
//            } else {
//                fibonacciNumbers[i] = fibonacciNumbers[i - 1] + fibonacciNumbers[i - 2];
//            }
//        }
//        System.out.println(fibonacciNumbers);
//        for (int i = 0; i < fibonacciNumbers.length; i++) {
//            System.out.println(fibonacciNumbers[i]);
//        }



//        Quiz 01 test codes:
//        int truncatedValue = (int)2.56;
//        System.out.println(truncatedValue);
//
//        String word = "circumlocution";
//        String partWord = word.substring(0, 3);
//        System.out.println(partWord.length() + " " + partWord + word.substring(3, word.length()));
//
//        System.out.println(3 + 7 + 15.0 / 3 + 2 * 0);

//        int a = 3;
//        int b = a * 4;
//        int c = a % b;
//        System.out.println("a: " + a + " b: " + b + " c: " + c);
//
//        System.out.println(Math.min(1, 0) + Math.max(1, 0));

//        int a = 10;
//        int b = a - 5 * 2 + 12;
//        int c = 10 / 2;
//        a = c;
//        System.out.println("a: " + a);

    }

    public static int stringCompare (String s1, String s2) {
        int l1 = s1.length();
        int l2 = s2.length();

        for (int i = 0; i < Math.min(l1, l2); i++) {
            int s1Char = (int) s1.charAt(i);
            int s2Char = (int) s2.charAt(i);

            if (s1Char != s2Char) {
                return s1Char - s2Char;
            }
        }

        if (l1 != l2) {
            return l1 - l2;
        } else {
            return 0;
        }
    }

}
