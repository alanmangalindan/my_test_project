package mytestpackage.innertestpackage;

public class Quiz2TestCodes {

    public static void main(String[] args) {
        Quiz2TestCodes q = new Quiz2TestCodes();
        q.start();
//        q.getNewNumber(1, 2);
    }

    public void start() {
//        int count = 0, limit = 10;
//        System.out.println("a: " + ((count < 10) || (limit < 0)));
//        System.out.println("b: " + ((count == 0) && (limit < 20)));

        int number = 3;
        int count = 0;
        while (count < 5) {
            int printValue = count + 1;
            System.out.println(printValue + " x " + number + " = "
                    + printValue * number);
            count++;
        }

        int[] nums1 = {1, 2, 3, 66, 4, 21};
        int[] nums2 = getArray(nums1, 6);
        System.out.println(nums2); // probably convert int array to char first..

    }

    private void getNewNumber(int x, int y) {
        x += 40;
        y += 40;
        int ans = 0;
        if (x == y) {
            ans = x + 10;
            System.out.println(ans);
        }
    }

    private int[] getArray(int[] a1, int a2) {
        int[] result = new int[a1.length];
        for (int i = 0; i < a1.length; i++) {
            result[i] = Math.max(a1[i], a2);
        }
        return result;
    }
}
