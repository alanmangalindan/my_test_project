package mytestpackage.innertestpackage;

import java.util.Stack;

public class Cat extends Animal {

    public int x = 10;

    public static int y = 100;

    public Cat() {
        super(250);
        this.setPrivateInt(1000);
        System.out.println(super.getPrivateInt());
        System.out.println(this.getPrivateInt());
        System.out.println(getPrivateInt());
//        this.privateInt = 300;
//        System.out.println(super.getPrivateInt());
//        System.out.println(this.getPrivateInt());
//        System.out.println(getPrivateInt());
//        System.out.println(privateInt);
    }

    @Override
    public int getPrivateInt() { // we can use public because public access is greater than protected. we cannot use private.
        return super.getPrivateInt();
    }


    public static void testClassMethod() {
        System.out.println("The static method in Cat");
    }

    public void testInstanceMethod() {
        System.out.println("The instance method in Cat");
    }

    public static void main(String[] args) {
        Cat myCat = new Cat();
        Animal myAnimal = myCat;
        Animal.testClassMethod(); // The static method in Animal
        myAnimal.testInstanceMethod(); // The instance method in Cat
        myCat.testInstanceMethod(); // The instance method in Cat
        myCat.testClassMethod(); // The static method in Cat
        myAnimal.testClassMethod(); // The static method in Animal
        System.out.println("myCat.x: " + myCat.x); // 10
        System.out.println("myAnimal.x: " + myAnimal.x); // 20
        System.out.println("myCat.y: " + myCat.y); // 100
        System.out.println("myAnimal.y: " + myAnimal.y); // 200
        System.out.println("Cat.y: " + Cat.y); // 100

        System.out.println("privateInt: "+ myCat.getPrivateInt());
        myCat.setPrivateInt(500);
        System.out.println("privateInt: "+ myCat.getPrivateInt());

        System.out.println("privateInt: "+ myAnimal.getPrivateInt());
        myAnimal.setPrivateInt(500);
        System.out.println("privateInt: "+ myAnimal.getPrivateInt());

//        System.out.println(myCat.privateInt); // can be done if privateInt variable is protected and not private
    }
}

// an example from https://docs.oracle.com/javase/tutorial/java/IandI/override.html