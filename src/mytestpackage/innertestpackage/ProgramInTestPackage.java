package mytestpackage.innertestpackage;

public class ProgramInTestPackage {

    private static int sampleInt;
    private static char sampleChar;
    public static String sampleString;

    public ProgramInTestPackage() {
    }

    public static void showVariables() {
        System.out.println("sampleInt: " + sampleInt + " sampleChar: " + sampleChar + " sampleString: " + sampleString);
    }

    public void setSampleInt(int i) {
        sampleInt = i;
    }

    public void setSampleChar(char sampleChar) {
        this.sampleChar = sampleChar;
    }

    public void setSampleString(String s) {
        sampleString = s;
    }

}
