package mytestpackage.innertestpackage;

public interface Printer {

    public void printDocument(String d);

    public int getEstimateMinutesRemaining();

    public String getError();

    default String printerInfo() {
        return "I am from the Printer Interface";
    }

    static String staticMethodInPrinterInterface() {
        return "I am from the Static Method in Printer Interface";
    }

}