package mytestpackage.innertestpackage;

public class PrintOmatic implements Printer {

    @Override
    public void printDocument(String d) {
        System.out.println(d);
    }

    @Override
    public int getEstimateMinutesRemaining() {
        return 0;
    }

    @Override
    public String getError() {
        return null;
    }

}
