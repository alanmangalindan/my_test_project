package mytestpackage.innertestpackage;

public interface NewPrinter extends Printer {

    @Override
   default String printerInfo() {
        return "I am from NewPrinter Interface";
    }

    static String staticMethodInPrinterInterface() {
        return "I am from the new Static Method in NewPrinter Interface";
    }

}
