package mytestpackage.innertestpackage;

public class PrinterOnly implements NewPrinter {

    @Override
    public void printDocument(String d) {
        System.out.println(d);
    }

    @Override
    public String getError() {
        return null;
    }

    @Override
    public String printerInfo() {
        return null;
    }

    @Override
    public int getEstimateMinutesRemaining() {
        return 0;
    }

}
