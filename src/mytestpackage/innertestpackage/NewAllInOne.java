package mytestpackage.innertestpackage;

public class NewAllInOne implements NewPrinter, Scanner {

    @Override
    public String getError() {
        return null;
    }

    @Override
    public int getEstimateMinutesRemaining() {
        return 0;
    }

    @Override
    public void printDocument(String d) {
        System.out.println(d);
    }

    @Override
    public String getDocument() {
        return null;
    }

    @Override
    public boolean jobsDone() {
        return false;
    }
}
