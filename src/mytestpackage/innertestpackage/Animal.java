package mytestpackage.innertestpackage;

public class Animal {

    public int x = 20;

    public static int y = 200;

    private int privateInt = 0;

    public Animal(int z) {
        this.privateInt = z;
    }

    public static void testClassMethod() {
        System.out.println("The static method in Animal");
    }
    public void testInstanceMethod() {
        System.out.println("The instance method in Animal");
    }

    protected int getPrivateInt() {
        return this.privateInt;
    }

    protected void setPrivateInt(int privateInt) {
        this.privateInt = privateInt;
    }
}


// an example from https://docs.oracle.com/javase/tutorial/java/IandI/override.html