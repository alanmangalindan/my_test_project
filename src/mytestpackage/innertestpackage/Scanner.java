package mytestpackage.innertestpackage;

public interface Scanner {

    final static String HP_ALL_IN_ONE = "HP Deskjet 3630";

    public String getDocument();

    public boolean jobsDone();

    public String getError();

}
