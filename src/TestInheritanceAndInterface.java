import mytestpackage.innertestpackage.*;

public class TestInheritanceAndInterface {

    public static void main(String[] args) {

//        Scanner eg2 = new PrintOmatic(); // not valid - PrintOmatic does not implement Scanner interface and cannot be converted to 'Scanner' reference type

        Printer eg3 = new OverpricedAllInOnePrinterfier();

//        OverpricedAllInOnePrinterfier eg4 = new Printer(); // not valid - an interface cannot be instantiated

//        Printer eg1 = new Scanner(); // not valid - an interface cannot be instantiated

        System.out.println("Printer eg3: " + eg3);

        System.out.println(eg3.getError()); // this method is from Printer Interface so no casting required
        System.out.println(eg3.getEstimateMinutesRemaining()); // this method is from Printer Interface so no casting required
        eg3.printDocument("Hello"); // this method is from Printer Interface so no casting required

        System.out.println(((OverpricedAllInOnePrinterfier) eg3).getDocument()); // cast to type of OverpricedAllInOnePrinterfier because method is from Scanner Interface

        System.out.println(((OverpricedAllInOnePrinterfier) eg3).jobsDone()); // cast to type of OverpricedAllInOnePrinterfier because method is from Scanner Interface

        System.out.println(OverpricedAllInOnePrinterfier.HP_ALL_IN_ONE);
        System.out.println(((OverpricedAllInOnePrinterfier) eg3).HP_ALL_IN_ONE);
        System.out.println(((Scanner) eg3).HP_ALL_IN_ONE);
//        System.out.println(eg3.HP_ALL_IN_ONE); // error - eg3 as a Printer reference type cannot see the constant HP_ALL_IN_ONE which is in the Scanner Interface


        Printer eg4 = new NewAllInOne();

        System.out.println("eg4.printerInfo(): " + eg4.printerInfo()); // I am from NewPrinter Interface
        System.out.println("eg4.staticMethodInPrinterInterface: " + " will not work!"); //eg4.staticMethodInPrinterInterface());
        System.out.println("((NewAllInOne) eg4).staticMethodInPrinterInterface: " + " will not work!"); // ((NewAllInOne) eg4).staticMethodInPrinterInterface());
        System.out.println("Printer.staticMethodInPrinterInterface: " + Printer.staticMethodInPrinterInterface());
        System.out.println("NewPrinter.staticMethodInPrinterInterface: " + NewPrinter.staticMethodInPrinterInterface());
        System.out.println("((NewAllInOne) eg4).printerInfo(): " + ((NewAllInOne) eg4).printerInfo()); // I am from NewPrinter Interface
        System.out.println("((NewPrinter) eg4).printerInfo(): " + ((NewPrinter) eg4).printerInfo()); // I am from NewPrinter Interface

        System.out.println(((NewAllInOne) eg4).jobsDone()); // casted to new type because eg4 is of Printer type and does not have the Scanner methods
        System.out.println(((NewAllInOne) eg4).getDocument()); // casted to new type because eg4 is of Printer type and does not have the Scanner methods
        System.out.println(((Scanner) eg4).jobsDone()); // casted to new type because eg4 is of Printer type and does not have the Scanner methods
        System.out.println(((Scanner) eg4).getDocument()); // casted to new type because eg4 is of Printer type and does not have the Scanner methods

        System.out.println("eg4 instanceof NewAllInOne: " + (eg4 instanceof NewAllInOne));
        System.out.println("eg4 instanceof Printer: " + (eg4 instanceof Printer));
        System.out.println("eg4 instanceof Scanner: " + (eg4 instanceof Scanner));

        Scanner sc = new NewAllInOne();
        System.out.println("sc instanceof Scanner: " + (sc instanceof Scanner));
        System.out.println("sc instanceof Printer: " + (sc instanceof Printer));

        NewPrinter printer = new NewAllInOne();
        System.out.println("printer instanceof Printer: " + (printer instanceof Printer));

        NewPrinter printerOnly = new PrinterOnly();
        System.out.println("printerOnly instanceof Printer: " + (printerOnly instanceof Printer));
        System.out.println("printerOnly instanceof Scanner: " + (printerOnly instanceof Scanner));

    }

}
